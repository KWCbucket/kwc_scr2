﻿using System.Collections.Generic;
using System.Linq;

namespace KWC_SCR2
{
   /// <summary>
   /// Keep a count of the schools or clubs worked
   /// </summary>
   class CategoryProcessor
   { 

      public int CategoryCount = 0;  // Number of Individuals, Clubs or Schools worked
      
      /// <summary>
      ///Key = call of the Club or School
      ///Value = count of times this Club or School has been worked
      /// </summary>
      private SortedDictionary<string, int> categoryDictionary =
         new SortedDictionary<string, int>();


      private FormMain MainForm;
      private string categoryName; // Category of this processor (School, Clu or Individual)


      public CategoryProcessor(FormMain anchor, string CategoryName)
      {
         MainForm = anchor;
         categoryName = CategoryName;
         Initialize();
      }

      public void Initialize() 
      {
      }

      public void Reset()
      {
         categoryDictionary.Clear();
         CategoryCount = 0;
      }

      /// <summary>
      /// Increment the number of times a Club or School has been worked
      /// if this is the first time a Club or School has been worked increment 
      /// the count of Clubs or Schools worked
      /// </summary>
      /// <param name="theQSO"></param>
      public void IncrementCount(ref OneQSO theQSO)
      {
         int currentCount;
         if (categoryDictionary.TryGetValue(theQSO.OtherCall, out currentCount))
         {
            currentCount++;
            categoryDictionary[theQSO.OtherCall] = currentCount;
         }
         else
         {
            categoryDictionary.Add(theQSO.OtherCall, 1);
            CategoryCount++;
         }
      }

      public void Dump()
      {
         MainForm.Message(MessageType.Info, " ====== Begin " + categoryName + " Dump ======");

         foreach (KeyValuePair<string, int> aStation in categoryDictionary.ToList())
         {
            MainForm.Message(MessageType.Info, string.Format("{0,10} ({1})",
               aStation.Key, aStation.Value));
         }

         MainForm.Message(MessageType.Info, " ====== End " + categoryName + " Dump ======");

      }

   }
}