﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.ComponentModel;
/*
Update log:
2016-02-18 V2.0.3.1 - Do not add any points for contacts flagged as duplicates
2016-02-19 V2.0.3.3 - Invalid entry class message was not given all parameters
2016-02-19 V2.0.3.5 - Expand QSO count labels to allow 9,999 QSOs
2016-11-13 V2.0.3.6 - Use "PK" as an abbreviation for Digital
2017-03-31 V2.0.4.0 - Define 6 meters as 52000-54000
2018-10-27 V2.1.0.1 - Display Entry Class
2019-02-22 V3.0.0.0 - Place log/data files in \users\<username>\AppData\Local\KWC Software\KWC_SCR2

*/
namespace KWC_SCR2
{

    public enum QSOmode
    {
        Invalid,
        CW,
        Digital,
        Phone
    };

    public enum QSOband
    {
        [Description("Unknown Band")]
        BandInvalid,
        [Description("160 meters")]
        Band160m, // 1.8-2.9 MHz
        [Description("80 meters")]
        Band80m,  // 3.5-4.0 MHz
        [Description("40 meters")]
        Band40m,  // 7.0-7.3 MHz
        [Description("20 meters")]
        Band20m,  // 14.0-14.35 MHz
        [Description("15 meters")]
        Band15m,  // 21.0-21.45 MHz
        [Description("10 meters")]
        Band10m,  // 28.0-29.7 MHz
        [Description("6 meters")]
        Band6m,    // 50-54 MHz
        [Description("2 meters")]
        Band2m,    // 144-148 MHz
        [Description("1.25 meters")]
        Band125m,    // 219-225 MHz
        [Description("70 cm")]
        Band70cm,   // 420-450 MHz
        [Description("33 cm")]
        Band33cm    // 902-928 MHz


    };

    public enum MessageType
    {
        Info,
        Error,
        NewLog
    };

    public enum QSOErrorType
    {
        None = 0,
        InvalidBand = 1,
        InvalidMode = 2,
        InvalidCall = 4,
        InvalidQTH = 8,
        DuplicateQSO = 16,
        InvalidCategory = 32
    };

    public struct OneQSO
    {
        public QSOErrorType Error;
        public QSOband Band;
        public QSOmode Mode;
        public string OtherCall;
        public string OtherClass;
        public string OtherQTH;
    };

    public partial class FormMain : Form
    {


        // Classes to process Phone, Digital and CW QSO counts
        private ModeProcessor phoneModeProcessor = null;
        private ModeProcessor digitalModeProcessor = null;
        private ModeProcessor cwModeProcessor = null;

        // Class to process States and Provinces worked
        private StateProvinceProcessor stateProvinceProcessor = null;
        private string StateProvinceFileName = "";

        // Class to process countries
        private CountryProcessor countryProcessor = null;
        private string CountryFileName = "";

        // Classes to process Schools, Clubs and Individuals
        private CategoryProcessor schoolProcessor = null;
        private CategoryProcessor clubProcessor = null;
        private CategoryProcessor individualProcessor = null;



        private string LogFileName; // Contest log to process
        private static string statisticsFileName = null;
        StreamWriter statisticsFile = null;


        public int LogLineNumber = 0;

        private int errorCounter = 0;
        private int totalQSOPoints = 0;
        private int totalMultipliers = 0;
        private int claimedScore = 0;



        private StreamWriter messageLogFile;

        public FormMain()
        {
            InitializeComponent();
            Common.InitializeCommonData();
            InitializeStatisticsFile();

            txtCreatedBy.Text = string.Empty;
            lblErrorCount.Text = string.Empty;

            //Initialize processors for Phone, Digital and CW QSOs
            phoneModeProcessor = new ModeProcessor(this, QSOmode.Phone);
            digitalModeProcessor = new ModeProcessor(this, QSOmode.Digital);
            cwModeProcessor = new ModeProcessor(this, QSOmode.CW);

            // Initialize processors for Individuals, Clubs and Schools
            schoolProcessor = new CategoryProcessor(this, "Schools");
            clubProcessor = new CategoryProcessor(this, "Clubs");
            individualProcessor = new CategoryProcessor(this, "Individuals");


        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        public void Message(MessageType MsgType, string message)
        {
            //If the log file has not been initialized or this is a request to
            // initialize the log file close and reallocate it
            if ((MsgType == MessageType.NewLog) || (messageLogFile == null))
            {
                InitMessageFile();
            }
            else if (MsgType == MessageType.Error) //error message?
            {
                errorCounter += 1;
                lblErrorCount.Text = String.Format("{0:n0} errors (see log)", errorCounter);
                messageLogFile.Write("*ERR* ");
            }
            messageLogFile.WriteLine(message);
        }


        //(re)Initialize log file
        public void InitMessageFile()
        {
            //If a log file is active dispose of it
            if (messageLogFile != null)
            {
                messageLogFile.Dispose();
            }
            messageLogFile = new StreamWriter(Common.DataDirectory + @"\KWC_SCR2-log.txt")
            {
                AutoFlush = true // Write log line to disk immediately
            };
        }


        private void loadStatesProvincesFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadStateProvinceFile();
        }


        // Get the name of the state/province file to use. Default is the last used state/province file
        // which is saved in the project settings file
        private void loadStateProvinceFile()
        {
            StateProvinceFileName = Properties.Settings.Default.StateProvinceFile;

            if (StateProvinceFileName != "")
            {
                DialogResult askStateProvinceFile = MessageBox.Show("Use state/Province file " + StateProvinceFileName + "?",
                   "Select state/province file", MessageBoxButtons.YesNoCancel,
                   MessageBoxIcon.Question);

                if (askStateProvinceFile == DialogResult.Cancel)
                {
                    return;
                }
                if (askStateProvinceFile == DialogResult.Yes)
                {
                    goto OpenStateProvinceFile;
                }
            }

            openFileDialog1.Title = "Select state/province file";
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            StateProvinceFileName = openFileDialog1.FileName;

            OpenStateProvinceFile:
            Properties.Settings.Default.StateProvinceFile = StateProvinceFileName;
            Properties.Settings.Default.Save();

            // Initialize state/province processing by loading the state/province file
            stateProvinceProcessor = new StateProvinceProcessor(this, StateProvinceFileName);
        }


        private void loadCountriesFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            loadCountryFile();
        }

        // Get the name of the country file to use. Default is the last used country file
        // which is saved in the project settings file
        private void loadCountryFile()
        {
            CountryFileName = Properties.Settings.Default.CountryFile;

            if (CountryFileName != "")
            {
                DialogResult askCountryFile = MessageBox.Show("Use country file " + CountryFileName + "?",
                   "Select country file", MessageBoxButtons.YesNoCancel,
                   MessageBoxIcon.Question);

                if (askCountryFile == DialogResult.Cancel)
                {
                    return;
                }
                if (askCountryFile == DialogResult.Yes)
                {
                    goto OpenCountryFile;
                }
            }

            openFileDialog1.Title = "Select country file";
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            CountryFileName = openFileDialog1.FileName;

            OpenCountryFile:
            Properties.Settings.Default.CountryFile = CountryFileName;
            Properties.Settings.Default.Save();

            // Initialize country processing by loading the country file
            countryProcessor = new CountryProcessor(this, CountryFileName);
        }




        // Main loop to process the log file
        /// <summary>
        /// Process one log file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.Title = "Select log to process";
            if (openFileDialog1.ShowDialog() != DialogResult.OK)
            {
                return;
            }
            LogFileName = openFileDialog1.FileName;

            // Make sure that the State/Province file has been loaded
            if (stateProvinceProcessor == null)
            {
                loadStateProvinceFile();
            }

            // Make sure that the Country file has been loaded
            if (countryProcessor == null)
            {
                loadCountryFile();
            }

            newLog();
            Message(MessageType.Info, "Processing " + LogFileName);


            using (StreamReader logFileReader = new StreamReader(LogFileName))
            {
                string logLine;
                string[] logSplit;

                while (logFileReader.Peek() > -1)
                {
                    logLine = Common.Squeeze(logFileReader.ReadLine());
                    LogLineNumber++;

                    logSplit = logLine.ToUpper().Split();

                    switch (logSplit[0])  // Type of Cabrillo line (e.g. CALLSIGN:, QSO: etc)
                    {

                        case "CALLSIGN:":
                            lblEntryName.Text = logSplit[1];
                            break;

                        case "CATEGORY-STATION:":
                            txtEntryClass.Text = logSplit[1];
                            break;

                        case "CREATED-BY:":
                            txtCreatedBy.Text = logLine.Substring(11);
                            break;

                        case "CLAIMED-SCORE:":
                            if (logSplit.Length >= 2)
                            {
                                int.TryParse(logSplit[1], out claimedScore);
                                txtClaimedScore.Text = string.Format("{0:n0}", claimedScore);
                            }
                            break;

                        case "QSO:":

                            // if there is not enough data issue an error message and
                            // skip processing the rest of the line
                            if (logSplit.Length < 13)
                            {
                                Message(MessageType.Error,
                                 $"Insufficient data on line {LogLineNumber}");
                                break; //Do not process this line
                            }

                            processQSO(logSplit);
                            break;
                    }
                }
            }



            // Processing is complete, now print out the final score

            /*
             Phone, Digital, CW and Total QSO counts
            */
            lblPhoneQSOs.Text = String.Format("{0:n0}", phoneModeProcessor.QSOcount);
            lblDigitalQSOs.Text = String.Format("{0:n0}", digitalModeProcessor.QSOcount);
            lblCWQSOs.Text = String.Format("{0:n0}", cwModeProcessor.QSOcount);
            lblTotalQSOs.Text = String.Format("{0:n0}", phoneModeProcessor.QSOcount +
               digitalModeProcessor.QSOcount + cwModeProcessor.QSOcount);

            /*
             Phone, Digital, CW and Total QSO points
            */
            totalQSOPoints = phoneModeProcessor.QSOcount +
                             (digitalModeProcessor.QSOcount * 2) +
                             (cwModeProcessor.QSOcount * 2);
            lblPhonePoints.Text = String.Format("{0:n0}", phoneModeProcessor.QSOcount);
            lblDigitalPoints.Text = String.Format("{0:n0}", digitalModeProcessor.QSOcount * 2);
            lblCWPoints.Text = String.Format("{0:n0}", cwModeProcessor.QSOcount * 2);
            lblQSOPoints.Text = String.Format("{0:n0}", totalQSOPoints);


            /*
             State, Province and DX multipliers
            */
            lblStateQSOs.Text = String.Format("{0:n0}", stateProvinceProcessor.StatesWorked);
            lblProvinceQSOs.Text = String.Format("{0:n0}", stateProvinceProcessor.ProvincesWorked);
            lblCountryQSOs.Text = String.Format("{0:n0}", countryProcessor.CountriesWorkedCount);

            /*
             State, Province and DX multiplier points
            */
            lblStatePoints.Text = String.Format("{0:n0}", stateProvinceProcessor.StatesWorked);
            lblProvincePoints.Text = String.Format("{0:n0}", stateProvinceProcessor.ProvincesWorked);
            lblCountryPoints.Text = String.Format("{0:n0}", countryProcessor.CountriesWorkedCount);

            /*
             Club and School multipliers
            */
            lblSchoolQSOs.Text = String.Format("{0:n0}", schoolProcessor.CategoryCount);
            lblClubQSOs.Text = String.Format("{0:n0}", clubProcessor.CategoryCount);

            /*
             Club and School multiplier points
            */
            lblClubPoints.Text = String.Format("{0:n0}", clubProcessor.CategoryCount * 2);
            lblSchoolPoints.Text = String.Format("{0:n0}", schoolProcessor.CategoryCount * 5);

            /*
             Calculate total multipliers and calculated score
            */
            totalMultipliers = stateProvinceProcessor.StatesWorked + stateProvinceProcessor.ProvincesWorked +
               countryProcessor.CountriesWorkedCount +
               (clubProcessor.CategoryCount * 2) + (schoolProcessor.CategoryCount * 5);
            lblTotalMultipliers.Text = String.Format("{0:n0}", totalMultipliers);
            txtCalculatedScore.Text = String.Format("{0:n0}", totalQSOPoints * totalMultipliers);

            /*
             * Output band/mode QSO counts to statistics file 
            */
            statisticsFile.Write(lblEntryName.Text + ",");
            int[] phoneQSOs = phoneModeProcessor.GetQSOsByBand();
            for (int i = 0; i < phoneQSOs.Length; i++)
            {
                statisticsFile.Write(phoneQSOs[i] + ",");
            }

            int[] cwQSOs = cwModeProcessor.GetQSOsByBand();
            for (int i = 0; i < cwQSOs.Length; i++)
            {
                statisticsFile.Write(cwQSOs[i] + ",");
            }

            int[] digitalQSOs = digitalModeProcessor.GetQSOsByBand();
            for (int i = 0; i < digitalQSOs.Length; i++)
            {
                statisticsFile.Write(digitalQSOs[i] + ",");
            }


            statisticsFile.WriteLine(string.Format("{0:g}", DateTime.Now));


        }

        /// <summary>
        /// A new log file is being started - initialize all per log variables
        /// </summary>
        private void newLog()
        {
            lblEntryName.Text = "";
            txtEntryClass.Text = "";
            txtClaimedScore.Text = "";
            lblErrorCount.Text = "";
            txtCreatedBy.Text = "";
            LogLineNumber = 0;
            totalQSOPoints = 0;
            errorCounter = 0;
            claimedScore = 0;
            phoneModeProcessor.Reset();
            cwModeProcessor.Reset();
            digitalModeProcessor.Reset();
            stateProvinceProcessor.Reset();
            countryProcessor.Reset();
            clubProcessor.Reset();
            schoolProcessor.Reset();
            individualProcessor.Reset();
            if (autoresetToolStripMenuItem.Checked)
            {
                Message(MessageType.NewLog, "");
            }
        }


        /// <summary>
        /// A QSO is being processed - extract the necessary fields  and increment appropriate counts
        /// </summary>
        /// <param name="logSplit">The log file line with blank delimited fields split 
        /// into an array of strings</param>
        private void processQSO(string[] logSplit)
        {
            OneQSO ThisQSO = new OneQSO();
            ThisQSO.Error = QSOErrorType.None;  // Assume no errors
            //
            // Get the frequency band
            //
            int freq = 0;
            int.TryParse(logSplit[1], out freq);

            if (freq >= 1800 && freq <= 2900)
            {
                ThisQSO.Band = QSOband.Band160m;
            }
            else if (freq >= 3500 && freq <= 4000)
            {
                ThisQSO.Band = QSOband.Band80m;
            }
            else if (freq >= 7000 && freq <= 7300)
            {
                ThisQSO.Band = QSOband.Band40m;
            }
            else if (freq >= 14000 && freq <= 14350)
            {
                ThisQSO.Band = QSOband.Band20m;
            }
            else if (freq >= 21000 && freq <= 21450)
            {
                ThisQSO.Band = QSOband.Band15m;
            }
            else if (freq >= 28000 && freq <= 29700)
            {
                ThisQSO.Band = QSOband.Band10m;
            }
            else if ((freq >= 50000 && freq <= 54000) || (freq == 50))
            {
                ThisQSO.Band = QSOband.Band6m;
            }
            else if (freq >= 144 && freq <= 148)
            {
                ThisQSO.Band = QSOband.Band2m;
            }
            else if (freq >= 219 && freq <= 225)
            {
                ThisQSO.Band = QSOband.Band125m;
            }
            else if (freq >= 420 && freq <= 450)
            {
                ThisQSO.Band = QSOband.Band70cm;
            }
            else if (freq >= 902 && freq <= 928)
            {
                ThisQSO.Band = QSOband.Band33cm;
            }


            else
            {
                ThisQSO.Band = QSOband.BandInvalid;
                ThisQSO.Error += (int)QSOErrorType.InvalidBand;
                Message(MessageType.Error, "Line " + LogLineNumber + " unknown band for frequency " + logSplit[1]);
            }
            ThisQSO.OtherCall = logSplit[9]; // Call letters of station being worked
            ThisQSO.OtherClass = logSplit[11]; // Class (School, Club, Individual) of station being worked
            ThisQSO.OtherQTH = logSplit[12];  // Location (State abbreviation or "DX") of station being worked




            //
            // Get the mode and calculate the QSO points for this QSO
            //
            switch (logSplit[2])
            {
                case "PH":
                case "SSB":
                case "FM":
                    ThisQSO.Mode = QSOmode.Phone;
                    phoneModeProcessor.ProcessQSO(ref ThisQSO);
                    break;

                case "CW":
                    ThisQSO.Mode = QSOmode.CW;
                    cwModeProcessor.ProcessQSO(ref ThisQSO);
                    break;

                case "RY":
                case "BP":
                case "PSK":
                case "PSK31":
                case "PK":
                case "FT8":
                    ThisQSO.Mode = QSOmode.Digital;
                    digitalModeProcessor.ProcessQSO(ref ThisQSO);
                    break;

                default:
                    ThisQSO.Mode = QSOmode.Invalid;
                    Message(MessageType.Error, "Line " + LogLineNumber + " invalid mode " + logSplit[2]);
                    break;
            }

            if (ThisQSO.Error != QSOErrorType.None)
            {
                return;
            }

            // Calculate State, Province and DX multipliers
            if (ThisQSO.OtherQTH == "DX")
            {
                // Count countries worked
                countryProcessor.IncrementCount(ref ThisQSO);
            }
            else
            {
                // Count States/Provinces worked
                stateProvinceProcessor.IncrementCount(ref ThisQSO);
            }

            // Count Clubs/Schools worked

            switch (ThisQSO.OtherClass)
            {

                case "C":  // Club
                    clubProcessor.IncrementCount(ref ThisQSO);
                    break;

                case "S":  // School
                    schoolProcessor.IncrementCount(ref ThisQSO);
                    break;

                case "I":  // Individual
                    individualProcessor.IncrementCount(ref ThisQSO);
                    break;


                default:
                    // Invalid entry class
                    Message(MessageType.Error, String.Format("Log line {0}: Invalid entry category {1} for {2}",
                       LogLineNumber,
                       ThisQSO.OtherClass,
                       ThisQSO.OtherCall));
                    ThisQSO.Error += (int)QSOErrorType.InvalidCategory;
                    break;
            }
        }

        private void countriesListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            countryProcessor.Dump();
        }

        private void statesListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stateProvinceProcessor.Dump("S");
        }

        private void provincesListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            stateProvinceProcessor.Dump("P");
        }

        private void nowToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Message(MessageType.NewLog, "");
        }

        private void cWToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cwModeProcessor.Dump();
        }

        private void digitalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            digitalModeProcessor.Dump();
        }

        private void phoneToolStripMenuItem_Click(object sender, EventArgs e)
        {
            phoneModeProcessor.Dump();
        }

        private void schoolToolStripMenuItem_Click(object sender, EventArgs e)
        {
            schoolProcessor.Dump();
        }

        private void clubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            clubProcessor.Dump();
        }

        private void individualToolStripMenuItem_Click(object sender, EventArgs e)
        {
            individualProcessor.Dump();
        }

        private void InitializeStatisticsFile()
        {
            bool newStatisticsFile = true;

            statisticsFileName = Common.DataDirectory + @"\KWC_SCR2-statistics.csv";
            newStatisticsFile = !(File.Exists(statisticsFileName)); // true if file is being created

            statisticsFile = new StreamWriter(statisticsFileName, true);


            // If we are creating a new file write the header row containing the column names
            if (newStatisticsFile) 
            {
                statisticsFile.Write("Call,");
                for (int i = 0; i < Common.numberOfBands; i++)
                {
                    statisticsFile.Write(Common.GetDescription((QSOband)i) + " Phone,");
                }

                for (int i = 0; i < Common.numberOfBands; i++)
                {
                    statisticsFile.Write(Common.GetDescription((QSOband)i) + " CW,");
                }

                for (int i = 0; i < Common.numberOfBands; i++)
                {
                    statisticsFile.Write(Common.GetDescription((QSOband)i) + " Digital,");
                }
                statisticsFile.WriteLine("Timestamp");
            }
        }



        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            statisticsFile.Close();
            statisticsFile.Dispose();
        }

        // Double-click on the calculated score to copy it to the clipboard
        private void txtCalculatedScore_DoubleClick(object sender, EventArgs e)
        {
            txtCalculatedScore.SelectionStart = 0;
            txtCalculatedScore.SelectionLength = txtCalculatedScore.Text.Length;
            Clipboard.SetText(txtCalculatedScore.Text);
        }

        private void txtClaimedScore_DoubleClick(object sender, EventArgs e)
        {
            txtClaimedScore.SelectionStart = 0;
            txtClaimedScore.SelectionLength = txtClaimedScore.Text.Length;
            Clipboard.SetText(txtClaimedScore.Text);
        }

        private void txtCreatedBy_DoubleClick(object sender, EventArgs e)
        {
            txtCreatedBy.SelectionStart = 0;
            txtCreatedBy.SelectionLength = txtCreatedBy.Text.Length;
            Clipboard.SetText(txtCreatedBy.Text);
        }

        private void logCountryAssignmentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logCountryAssignmentToolStripMenuItem.Checked = !(logCountryAssignmentToolStripMenuItem.Checked);
        }


        private void txtEntryClass_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            txtEntryClass.SelectionStart = 0;
            txtEntryClass.SelectionLength = txtEntryClass.Text.Length;
            Clipboard.SetText(txtEntryClass.Text);
        }

    }
}