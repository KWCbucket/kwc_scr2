﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KWC_SCR2
{
   class CountryProcessor
   {

      struct CallCountry
      {
         public string partialCall;
         public string country;
      };

      public int CountriesWorkedCount = 0;

      /// <summary>
      /// Key = country name
      /// Value = counter of contacts for this country
      /// </summary>
      private SortedDictionary<string, int> countryDictionary =
              new SortedDictionary<string, int>();


      /// <summary>
      /// Key = exact call
      /// Value = Country name (to look up in CountryDictionary)
      /// </summary>
      private Dictionary<string, string> exactCallDictionary =
              new Dictionary<string, string>();

      private List<CallCountry> partialCallList = new List<CallCountry>();

      private FormMain MainForm;
      private string countryFileName = "";
      private string callToFind;

      public CountryProcessor(FormMain anchor, string countryFile)
      {
         MainForm = anchor;
         countryFileName = countryFile;
         Initialize();
      }

      public void Initialize()
      {
         using (StreamReader countryReader = new StreamReader(countryFileName))
         {
            string countryFileLine;
            string[] countryLineSplit;

            while (countryReader.Peek() > -1) // While not EOF
            {
               countryFileLine = countryReader.ReadLine();

               // Ignore all blank or comment lines
               if ((countryFileLine != "") && (countryFileLine.Substring(0, 1) != "#"))
               {
                  countryFileLine = countryFileLine.Replace("\"", "");

                  // Split the country file line into chunks
                  // chunk [0] is the country call prefix (or complete call preceded by "=")
                  // chunk [1] is the country name
                  countryLineSplit = countryFileLine.Split(',');

                  // Add the country name to the dictionary if it is not already there
                  if (!countryDictionary.ContainsKey(countryLineSplit[1]))
                  {
                     countryDictionary.Add(countryLineSplit[1], 0);
                  }

                  // If the call starts with a "=" it must be matched exactly. The ExactCallDictionary
                  // provides a cross reference from the call to the country name
                  if (countryLineSplit[0].Substring(0, 1) == "=")
                  {
                     exactCallDictionary.Add(countryLineSplit[0].Substring(1), countryLineSplit[1]);
                  }
                  else
                  {
                     CallCountry newCallCountry = new CallCountry();
                     newCallCountry.partialCall = countryLineSplit[0];
                     newCallCountry.country = countryLineSplit[1];
                     partialCallList.Add(newCallCountry);
                  }
               }
            }
         }
         // Country file has been loaded, now sort the partial call list by length descending
         partialCallList.Sort(SortByLength);
      }

      /// <summary>
      /// Reset the counters for each country to zero at the start of a new log
      /// </summary>
      public void Reset()
      {

         CountriesWorkedCount = 0; // Reset count of countries worked

         foreach (KeyValuePair<string, int> aCountry in countryDictionary.ToList())
         {
            countryDictionary[aCountry.Key] = 0;
         }
      }

      /// <summary>
      /// Increment the number of times a Country has been worked
      /// if this is the first time a Country has been worked increment 
      /// the count of Countries
      /// </summary>
      /// <param name="theQSO"> Current QSO being processed</param>
      public void IncrementCount(ref OneQSO theQSO)
      {
         string theCountry;

         // If the station being worked is in the exactCallDictionary then the dictionary
         // will return the country name
         if (!exactCallDictionary.TryGetValue(theQSO.OtherCall, out theCountry))
         {
            callToFind = theQSO.OtherCall;
            theCountry = partialCallList.Find(findPartialCall).country;
            if (theCountry == null)
            {
               MainForm.Message(MessageType.Error,
                  String.Format("Log line {0}: Unknown country for call {1}",
                  MainForm.LogLineNumber, theQSO.OtherCall));
               theQSO.Error += (int)QSOErrorType.InvalidCall;
               return;
            }

            // theCountry contains the name of the country being processed. Look it up
            // in countryDictionary and increment the QSO count for this country. 
            int countryQSOs = countryDictionary[theCountry];
            countryQSOs++;
            countryDictionary[theCountry] = countryQSOs;

            // if this is the first QSO with a country increment the countries worked count
            // do not count USA or Canada as DX countries
            if ( (countryQSOs == 1)  /* && (theCountry != "USA") && (theCountry != "Canada") */ )
            {
               CountriesWorkedCount++; 
            }

            if (MainForm.logCountryAssignmentToolStripMenuItem.Checked)
            {
               MainForm.Message(MessageType.Info, String.Format("{0,10} {1}",
                  theQSO.OtherCall, theCountry));
            }


         }
      }

      private bool findPartialCall(CallCountry theCall)
      {
         if (callToFind.Length < theCall.partialCall.Length)
         {
            return false;
         }
         return (callToFind.Substring(0, theCall.partialCall.Length) == theCall.partialCall);
      }

      private int SortByLength(CallCountry x, CallCountry y)
      {
         return y.partialCall.Length.CompareTo(x.partialCall.Length);
      }

      /// <summary>
      /// Dump the countries list to the log
      /// </summary>
      public void Dump()
      {
         MainForm.Message(MessageType.Info, "==========     Begin country list dump ==========");
         foreach (KeyValuePair<string, int> aCountry in countryDictionary.ToList())
         {
            if (aCountry.Value > 0)
            {
               MainForm.Message(MessageType.Info, String.Format("{0} ({1})",
                       aCountry.Key, aCountry.Value));
            }
         }
         MainForm.Message(MessageType.Info, "==========     End country list dump ==========");
      }
   }
}