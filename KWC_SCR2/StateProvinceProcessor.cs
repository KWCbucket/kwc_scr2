﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace KWC_SCR2
{
   class StateProvinceProcessor
   {
      struct StateProvinceDef
      {
         public string spName;
         public string spType; // S=state,  P=province
         public int qsoCount;
      };

      private SortedDictionary<string, StateProvinceDef> stateProvinceDictionary =
         new SortedDictionary<string, StateProvinceDef>();

      public int StatesWorked = 0;
      public int ProvincesWorked = 0;
      private FormMain MainForm;
      private string stateProvinceFileName = "";


      public StateProvinceProcessor(FormMain anchor, string stateProvinceFile)
      {
         MainForm = anchor;
         stateProvinceFileName = stateProvinceFile;
         Initialize();
      }

      // Use data from the StateProvince file to build a sorted dictionary
      public void Initialize()
      {
         using (StreamReader stateProvinceReader = new StreamReader(stateProvinceFileName))
         {
            string spLine;
            string[] spSplit;
            string stateOrProvince; // S=State, P=Province
            StateProvinceDef spDef;


            while (stateProvinceReader.Peek() > -1)
            {
               spLine = stateProvinceReader.ReadLine();
               if ((spLine != "") && (spLine.Substring(0, 1) != "#"))
               {
                  // Split State/Province definition into chunks separated by spaces
                  // chunk [0] is the type ("State" or "Province")
                  // chunk [1] is the abbreviation (and optional aliases each preceeded by "=")
                  // chunk [2] is the state or province name
                  spSplit = Common.Squeeze(spLine).Split();
                  if (spSplit[0] == "Province")
                  {
                     stateOrProvince = "P";
                  }
                  else
                  {
                     stateOrProvince = "S";
                  }

                  spDef.spName = spSplit[2];
                  spDef.spType = stateOrProvince;
                  spDef.qsoCount = 0;

                  // If the state/province abbreviation contains a "=" then it is a primary
                  // and a set of aliases:
                  // P=A1=A2=...An
                  // in the example above P is the primary abbreviation and A1...An are the 
                  // aliases. All of the QSOs made using aliases are accumulated under primsry
                  // The entry for all the aliases will have the primary abbreviation proceded
                  // a "=" in the name field so given the Alias we can easily look up the primary abbreviation

                  if (spSplit[1].Contains("="))
                  {
                     string[] spAlias = spSplit[1].Split('=');  //split primary and Alias
                     stateProvinceDictionary.Add(spAlias[0], spDef); //Primary

                     spDef.spName = "=" + spAlias[0];
                     for (int i = 1; i <= spAlias.Length - 1; i++)
                     {
                        stateProvinceDictionary.Add(spAlias[i], spDef); //Alias
                     }
                  }
                  else
                  {
                     stateProvinceDictionary.Add(spSplit[1], spDef);
                  }
               }
            }
         }
      }


      /// <summary>
      /// Reset the counters for each state/province to zero at the start of a new log
      /// </summary>
      public void Reset()
      {
         foreach (KeyValuePair<string, StateProvinceDef> aStateProvince in stateProvinceDictionary.ToList())
         {
            StateProvinceDef modifiedStateProvince = aStateProvince.Value;
            modifiedStateProvince.qsoCount = 0;
            stateProvinceDictionary[aStateProvince.Key] = modifiedStateProvince;
         }
         StatesWorked = 0;
         ProvincesWorked = 0;
      }

      /// <summary>
      /// Increment the number of times a State or Province has been worked
      /// if this is the first time a State or Province has been worked increment 
      /// the count of States or Provinces worked
      /// </summary>
      /// <param name="theQSO"></param>
      public void IncrementCount(ref OneQSO theQSO)
      {
         StateProvinceDef modifiedStateProvince;
         string theStateProvince = theQSO.OtherQTH;
         if (stateProvinceDictionary.TryGetValue(theStateProvince, out modifiedStateProvince))
         {
            if (modifiedStateProvince.spName.Substring(0,1)=="=")
            {
               theStateProvince = modifiedStateProvince.spName.Substring(1);
               modifiedStateProvince = stateProvinceDictionary[theStateProvince];
            }
            modifiedStateProvince.qsoCount++;
            if (modifiedStateProvince.qsoCount == 1)  // First time that this State/Province has been worked
            {
               if (modifiedStateProvince.spType == "S")
               {
                  StatesWorked++;
               }
               else
               {
                  ProvincesWorked++;
               }
            }
            stateProvinceDictionary[theStateProvince] = modifiedStateProvince;
         }
         else
         {
            MainForm.Message(MessageType.Error, String.Format("Log line {0}: Invalid state/province {1}",
               MainForm.LogLineNumber, theQSO.OtherQTH));
            theQSO.Error += (int)QSOErrorType.InvalidQTH;
         }
      }

      public void Dump(string dumpType)
      {
         MainForm.Message(MessageType.Info, "==========     Begin state/province list dump ==========");
         foreach (KeyValuePair<string, StateProvinceDef> aStateProvince in stateProvinceDictionary.ToList())
         {
            if ((aStateProvince.Value.spType==dumpType) && (aStateProvince.Value.qsoCount>0))
            {
               StateProvinceDef modifiedStateProvince = aStateProvince.Value;
               modifiedStateProvince.qsoCount = 0;
               stateProvinceDictionary[aStateProvince.Key] = modifiedStateProvince;
               MainForm.Message(MessageType.Info, String.Format("{0} {1} ({2})",
                  aStateProvince.Value.spType,
                  aStateProvince.Value.spName,
                  aStateProvince.Value.qsoCount)); 
            }
         }
         MainForm.Message(MessageType.Info, "==========     End state/province list dump ==========");
      }
   }
}