﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;



namespace KWC_SCR2
{
    static class Common
    {
        public static int numberOfBands;
        public static string DataDirectory = "";  // directory for data file


        public static void InitializeCommonData()
        {
            numberOfBands = Enum.GetNames(typeof(QSOband)).Length;

            DataDirectory = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)+ "\\KWC Software\\KWC_SCR2";
            Directory.CreateDirectory(DataDirectory); // Create data directory if it does not already exist
        }
    


        public static string GetDescription(object enumValue)
        {
            FieldInfo aField = enumValue.GetType().GetField(enumValue.ToString());

            if (aField != null)
            {
                object[] attrs = aField.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }

            return "? unknown ?";
        }


        /// <summary>
        /// Remove all tab characters and repeated blanks
        /// </summary>
        /// <param name="Unsquozen">
        /// input string to be processed</param>
        /// <returns>Input string with all tabs and repeated blanks removed</returns>
        public static string Squeeze(string Unsquozen)
        {
            string Squeezed = Unsquozen.Replace("\t", " ").Trim(); // remove tab characters and spaces at the beginning and end
            while (Squeezed.Contains("  "))
            {
                Squeezed = Squeezed.Replace("  ", " ");
            }
            return Squeezed;
        }


    }
}
