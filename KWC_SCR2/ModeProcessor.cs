﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace KWC_SCR2
{


   /// <summary>
   /// 
   /// </summary>
   class ModeProcessor
   {

      public int QSOcount = 0;

      private FormMain MainForm;
      private string modeName;
      private QSOmode mode;

      /// <summary>
      /// Sorted dictionary of QSOs made by a call on each band
      /// Key: - complete call of station worked
      /// Value: - Array (one element per band) of number of QSOs made with the call in the key. Index of
      ///          the band being processed is described by the QSOband enum
      /// </summary>
      private SortedDictionary<string, int[]> modeDictionary =
        new SortedDictionary<string, int[]>();

      public ModeProcessor(FormMain anchor, QSOmode modeType)
      {
         MainForm = anchor;
         mode = modeType;
         modeName = mode.ToString();
         Initialize();
      }

      public void Initialize()
      {
      }

      public void Reset()
      {
         QSOcount = 0;
         modeDictionary.Clear();
      }


      public void ProcessQSO(ref OneQSO theQSO)
      {
         int[] theBands;
         if (modeDictionary.TryGetValue(theQSO.OtherCall, out theBands))
         {
            // The call has been worked at least once on this mode. If it has been worked
            // on this band then it is a duplicate QSO
            if (theBands[(int)theQSO.Band] > 0)
            {
               // The call has already been worked on this band and mode
               MainForm.Message(MessageType.Error,
                  String.Format("Log line {0}: Station {1} has already been worked on {2} {3}",
                     MainForm.LogLineNumber,
                     theQSO.OtherCall,
                     Common.GetDescription(theQSO.Band),
                     modeName));
               theQSO.Error += (int)QSOErrorType.DuplicateQSO;
               return;
            }
            theBands[(int)theQSO.Band]++;
            modeDictionary[theQSO.OtherCall] = theBands; // Update the QSO count this band/mode
            QSOcount++;
         }
         else
         {
            // The call is not in the mode dictionary then add it
            theBands = new int[Common.numberOfBands];

            for (int i = 0; i < Common.numberOfBands; i++)
            {
               theBands[i] = 0;
            }
            theBands[(int)theQSO.Band] = 1; // This station has been worked once on this band
            modeDictionary.Add(theQSO.OtherCall, theBands);
            QSOcount++;
         }
      }

      public void Dump()
      {
         MainForm.Message(MessageType.Info, " ====== Begin " + modeName + " Dump ======");
         foreach (KeyValuePair<string, int[]> aStation in modeDictionary.ToList())
         {
            StringBuilder theMsg = new StringBuilder("");
            theMsg.AppendFormat("{0,10} ", aStation.Key);

            for (int i = 0; i < Common.numberOfBands; i++)
            {
               if (aStation.Value[i] > 0)
               {
                  theMsg.AppendFormat("{0} ({1})  ",
                                     Common.GetDescription((QSOband)i), aStation.Value[i]);
               }
            }
            MainForm.Message(MessageType.Info, theMsg.ToString());
         }
         MainForm.Message(MessageType.Info, " ====== End " + modeName + " Dump ======");
      }

      /// <summary>
      /// Count the number of QSOs made on each band. Go through dictionary of QSO counts
      /// by call and add number of QSOs to total by band
      /// </summary>
      /// <returns>array of integer counters, one counter per band
      /// </returns>
      public int[] GetQSOsByBand()
      {
         // Zero all the counters
         int[] bandCounts = new int[Common.numberOfBands];
         for (int i = 0; i < Common.numberOfBands; i++)
			{
			 bandCounts[i]=0;
			}


         foreach (KeyValuePair<string, int[]> aStation in modeDictionary.ToList())
         {

            for (int i = 0; i < Common.numberOfBands; i++)
            {
               bandCounts[i] += aStation.Value[i];
            }
         }
         return bandCounts;
      }



   }
}