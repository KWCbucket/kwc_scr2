﻿namespace KWC_SCR2
{
   partial class FormMain
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose(bool disposing)
      {
         if (disposing && (components != null))
         {
            components.Dispose();
         }
         base.Dispose(disposing);
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadStatesProvincesFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadCountriesFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dumpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.countriesListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statesprovincesListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.provincesListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cWToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.digitalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.phoneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.schoolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clubToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.individualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.debugToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resetLogFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.autoresetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logCountryAssignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblEntryName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblPhoneQSOs = new System.Windows.Forms.Label();
            this.lblDigitalQSOs = new System.Windows.Forms.Label();
            this.lblCWQSOs = new System.Windows.Forms.Label();
            this.lblTotalQSOs = new System.Windows.Forms.Label();
            this.lblPhonePoints = new System.Windows.Forms.Label();
            this.lblDigitalPoints = new System.Windows.Forms.Label();
            this.lblCWPoints = new System.Windows.Forms.Label();
            this.lblQSOPoints = new System.Windows.Forms.Label();
            this.lblErrorCount = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblStatePoints = new System.Windows.Forms.Label();
            this.lblProvincePoints = new System.Windows.Forms.Label();
            this.lblCountryPoints = new System.Windows.Forms.Label();
            this.lblClubPoints = new System.Windows.Forms.Label();
            this.lblSchoolPoints = new System.Windows.Forms.Label();
            this.lblStateQSOs = new System.Windows.Forms.Label();
            this.lblProvinceQSOs = new System.Windows.Forms.Label();
            this.lblCountryQSOs = new System.Windows.Forms.Label();
            this.lblClubQSOs = new System.Windows.Forms.Label();
            this.lblSchoolQSOs = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.lblTotalMultipliers = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lblCreatedBy = new System.Windows.Forms.Label();
            this.txtCreatedBy = new System.Windows.Forms.TextBox();
            this.txtCalculatedScore = new System.Windows.Forms.TextBox();
            this.txtClaimedScore = new System.Windows.Forms.TextBox();
            this.txtEntryClass = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.dumpToolStripMenuItem,
            this.debugToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(494, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 22);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadStatesProvincesFileToolStripMenuItem,
            this.loadCountriesFileToolStripMenuItem});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // loadStatesProvincesFileToolStripMenuItem
            // 
            this.loadStatesProvincesFileToolStripMenuItem.Name = "loadStatesProvincesFileToolStripMenuItem";
            this.loadStatesProvincesFileToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.loadStatesProvincesFileToolStripMenuItem.Text = "Load States/Provinces file";
            this.loadStatesProvincesFileToolStripMenuItem.Click += new System.EventHandler(this.loadStatesProvincesFileToolStripMenuItem_Click);
            // 
            // loadCountriesFileToolStripMenuItem
            // 
            this.loadCountriesFileToolStripMenuItem.Name = "loadCountriesFileToolStripMenuItem";
            this.loadCountriesFileToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.loadCountriesFileToolStripMenuItem.Text = "Load Countries file";
            this.loadCountriesFileToolStripMenuItem.Click += new System.EventHandler(this.loadCountriesFileToolStripMenuItem_Click);
            // 
            // dumpToolStripMenuItem
            // 
            this.dumpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.countriesListToolStripMenuItem,
            this.statesprovincesListToolStripMenuItem,
            this.provincesListToolStripMenuItem,
            this.modeToolStripMenuItem,
            this.classToolStripMenuItem});
            this.dumpToolStripMenuItem.Name = "dumpToolStripMenuItem";
            this.dumpToolStripMenuItem.Size = new System.Drawing.Size(52, 22);
            this.dumpToolStripMenuItem.Text = "Dump";
            // 
            // countriesListToolStripMenuItem
            // 
            this.countriesListToolStripMenuItem.Name = "countriesListToolStripMenuItem";
            this.countriesListToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.countriesListToolStripMenuItem.Text = "Countries worked";
            this.countriesListToolStripMenuItem.Click += new System.EventHandler(this.countriesListToolStripMenuItem_Click);
            // 
            // statesprovincesListToolStripMenuItem
            // 
            this.statesprovincesListToolStripMenuItem.Name = "statesprovincesListToolStripMenuItem";
            this.statesprovincesListToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.statesprovincesListToolStripMenuItem.Text = "States worked";
            this.statesprovincesListToolStripMenuItem.Click += new System.EventHandler(this.statesListToolStripMenuItem_Click);
            // 
            // provincesListToolStripMenuItem
            // 
            this.provincesListToolStripMenuItem.Name = "provincesListToolStripMenuItem";
            this.provincesListToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.provincesListToolStripMenuItem.Text = "Provinces worked";
            this.provincesListToolStripMenuItem.Click += new System.EventHandler(this.provincesListToolStripMenuItem_Click);
            // 
            // modeToolStripMenuItem
            // 
            this.modeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cWToolStripMenuItem,
            this.digitalToolStripMenuItem,
            this.phoneToolStripMenuItem});
            this.modeToolStripMenuItem.Name = "modeToolStripMenuItem";
            this.modeToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.modeToolStripMenuItem.Text = "Mode";
            // 
            // cWToolStripMenuItem
            // 
            this.cWToolStripMenuItem.Name = "cWToolStripMenuItem";
            this.cWToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.cWToolStripMenuItem.Text = "CW";
            this.cWToolStripMenuItem.Click += new System.EventHandler(this.cWToolStripMenuItem_Click);
            // 
            // digitalToolStripMenuItem
            // 
            this.digitalToolStripMenuItem.Name = "digitalToolStripMenuItem";
            this.digitalToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.digitalToolStripMenuItem.Text = "Digital";
            this.digitalToolStripMenuItem.Click += new System.EventHandler(this.digitalToolStripMenuItem_Click);
            // 
            // phoneToolStripMenuItem
            // 
            this.phoneToolStripMenuItem.Name = "phoneToolStripMenuItem";
            this.phoneToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.phoneToolStripMenuItem.Text = "Phone";
            this.phoneToolStripMenuItem.Click += new System.EventHandler(this.phoneToolStripMenuItem_Click);
            // 
            // classToolStripMenuItem
            // 
            this.classToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.schoolToolStripMenuItem,
            this.clubToolStripMenuItem,
            this.individualToolStripMenuItem});
            this.classToolStripMenuItem.Name = "classToolStripMenuItem";
            this.classToolStripMenuItem.Size = new System.Drawing.Size(167, 22);
            this.classToolStripMenuItem.Text = "Class";
            // 
            // schoolToolStripMenuItem
            // 
            this.schoolToolStripMenuItem.Name = "schoolToolStripMenuItem";
            this.schoolToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.schoolToolStripMenuItem.Text = "Schools";
            this.schoolToolStripMenuItem.Click += new System.EventHandler(this.schoolToolStripMenuItem_Click);
            // 
            // clubToolStripMenuItem
            // 
            this.clubToolStripMenuItem.Name = "clubToolStripMenuItem";
            this.clubToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.clubToolStripMenuItem.Text = "Clubs";
            this.clubToolStripMenuItem.Click += new System.EventHandler(this.clubToolStripMenuItem_Click);
            // 
            // individualToolStripMenuItem
            // 
            this.individualToolStripMenuItem.Name = "individualToolStripMenuItem";
            this.individualToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.individualToolStripMenuItem.Text = "Individuals";
            this.individualToolStripMenuItem.Click += new System.EventHandler(this.individualToolStripMenuItem_Click);
            // 
            // debugToolStripMenuItem
            // 
            this.debugToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetLogFileToolStripMenuItem,
            this.logCountryAssignmentToolStripMenuItem});
            this.debugToolStripMenuItem.Name = "debugToolStripMenuItem";
            this.debugToolStripMenuItem.Size = new System.Drawing.Size(54, 22);
            this.debugToolStripMenuItem.Text = "Debug";
            // 
            // resetLogFileToolStripMenuItem
            // 
            this.resetLogFileToolStripMenuItem.CheckOnClick = true;
            this.resetLogFileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nowToolStripMenuItem,
            this.autoresetToolStripMenuItem});
            this.resetLogFileToolStripMenuItem.Name = "resetLogFileToolStripMenuItem";
            this.resetLogFileToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.resetLogFileToolStripMenuItem.Text = "Reset log file";
            // 
            // nowToolStripMenuItem
            // 
            this.nowToolStripMenuItem.Name = "nowToolStripMenuItem";
            this.nowToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.nowToolStripMenuItem.Text = "Now";
            this.nowToolStripMenuItem.Click += new System.EventHandler(this.nowToolStripMenuItem_Click);
            // 
            // autoresetToolStripMenuItem
            // 
            this.autoresetToolStripMenuItem.Checked = true;
            this.autoresetToolStripMenuItem.CheckOnClick = true;
            this.autoresetToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoresetToolStripMenuItem.Name = "autoresetToolStripMenuItem";
            this.autoresetToolStripMenuItem.Size = new System.Drawing.Size(175, 22);
            this.autoresetToolStripMenuItem.Text = "Before each log file";
            // 
            // logCountryAssignmentToolStripMenuItem
            // 
            this.logCountryAssignmentToolStripMenuItem.Name = "logCountryAssignmentToolStripMenuItem";
            this.logCountryAssignmentToolStripMenuItem.Size = new System.Drawing.Size(202, 22);
            this.logCountryAssignmentToolStripMenuItem.Text = "Log country assignment";
            this.logCountryAssignmentToolStripMenuItem.Click += new System.EventHandler(this.logCountryAssignmentToolStripMenuItem_Click);
            // 
            // lblEntryName
            // 
            this.lblEntryName.AutoSize = true;
            this.lblEntryName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEntryName.Location = new System.Drawing.Point(151, 32);
            this.lblEntryName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblEntryName.Name = "lblEntryName";
            this.lblEntryName.Size = new System.Drawing.Size(114, 25);
            this.lblEntryName.TabIndex = 1;
            this.lblEntryName.Text = "Entry Name";
            this.lblEntryName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(61, 68);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(135, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Phone QSOs   x1 =";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(61, 87);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Digital  QSOs  x2 =";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(61, 107);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "CW     QSOs   x2 =";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(61, 126);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(135, 16);
            this.label5.TabIndex = 5;
            this.label5.Text = "<- QSOs   Points ->";
            // 
            // lblPhoneQSOs
            // 
            this.lblPhoneQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhoneQSOs.Location = new System.Drawing.Point(12, 68);
            this.lblPhoneQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPhoneQSOs.Name = "lblPhoneQSOs";
            this.lblPhoneQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblPhoneQSOs.Size = new System.Drawing.Size(45, 16);
            this.lblPhoneQSOs.TabIndex = 6;
            this.lblPhoneQSOs.Text = "0";
            // 
            // lblDigitalQSOs
            // 
            this.lblDigitalQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDigitalQSOs.Location = new System.Drawing.Point(12, 87);
            this.lblDigitalQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDigitalQSOs.Name = "lblDigitalQSOs";
            this.lblDigitalQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblDigitalQSOs.Size = new System.Drawing.Size(45, 16);
            this.lblDigitalQSOs.TabIndex = 7;
            this.lblDigitalQSOs.Text = "0";
            // 
            // lblCWQSOs
            // 
            this.lblCWQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCWQSOs.Location = new System.Drawing.Point(12, 107);
            this.lblCWQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCWQSOs.Name = "lblCWQSOs";
            this.lblCWQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCWQSOs.Size = new System.Drawing.Size(45, 16);
            this.lblCWQSOs.TabIndex = 8;
            this.lblCWQSOs.Text = "0";
            // 
            // lblTotalQSOs
            // 
            this.lblTotalQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalQSOs.Location = new System.Drawing.Point(12, 126);
            this.lblTotalQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalQSOs.Name = "lblTotalQSOs";
            this.lblTotalQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblTotalQSOs.Size = new System.Drawing.Size(45, 16);
            this.lblTotalQSOs.TabIndex = 9;
            this.lblTotalQSOs.Text = "0";
            // 
            // lblPhonePoints
            // 
            this.lblPhonePoints.AutoSize = true;
            this.lblPhonePoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhonePoints.Location = new System.Drawing.Point(185, 68);
            this.lblPhonePoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPhonePoints.Name = "lblPhonePoints";
            this.lblPhonePoints.Size = new System.Drawing.Size(16, 17);
            this.lblPhonePoints.TabIndex = 10;
            this.lblPhonePoints.Text = "0";
            // 
            // lblDigitalPoints
            // 
            this.lblDigitalPoints.AutoSize = true;
            this.lblDigitalPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDigitalPoints.Location = new System.Drawing.Point(184, 87);
            this.lblDigitalPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDigitalPoints.Name = "lblDigitalPoints";
            this.lblDigitalPoints.Size = new System.Drawing.Size(16, 17);
            this.lblDigitalPoints.TabIndex = 11;
            this.lblDigitalPoints.Text = "0";
            // 
            // lblCWPoints
            // 
            this.lblCWPoints.AutoSize = true;
            this.lblCWPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCWPoints.Location = new System.Drawing.Point(185, 107);
            this.lblCWPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCWPoints.Name = "lblCWPoints";
            this.lblCWPoints.Size = new System.Drawing.Size(16, 17);
            this.lblCWPoints.TabIndex = 12;
            this.lblCWPoints.Text = "0";
            // 
            // lblQSOPoints
            // 
            this.lblQSOPoints.AutoSize = true;
            this.lblQSOPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQSOPoints.Location = new System.Drawing.Point(185, 126);
            this.lblQSOPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblQSOPoints.Name = "lblQSOPoints";
            this.lblQSOPoints.Size = new System.Drawing.Size(16, 17);
            this.lblQSOPoints.TabIndex = 13;
            this.lblQSOPoints.Text = "0";
            // 
            // lblErrorCount
            // 
            this.lblErrorCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblErrorCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblErrorCount.ForeColor = System.Drawing.Color.Crimson;
            this.lblErrorCount.Location = new System.Drawing.Point(0, 348);
            this.lblErrorCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblErrorCount.Name = "lblErrorCount";
            this.lblErrorCount.Size = new System.Drawing.Size(494, 23);
            this.lblErrorCount.TabIndex = 14;
            this.lblErrorCount.Text = "Errors";
            this.lblErrorCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(61, 168);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "States            x1 =";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(263, 185);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Schools         x5 =";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(263, 169);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Clubs             x2 =";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(61, 185);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(126, 16);
            this.label9.TabIndex = 18;
            this.label9.Text = "Provinces       x1 =";
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(61, 203);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(127, 18);
            this.label10.TabIndex = 19;
            this.label10.Text = "DX Countries  x1 =";
            // 
            // lblStatePoints
            // 
            this.lblStatePoints.AutoSize = true;
            this.lblStatePoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatePoints.Location = new System.Drawing.Point(185, 168);
            this.lblStatePoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatePoints.Name = "lblStatePoints";
            this.lblStatePoints.Size = new System.Drawing.Size(16, 17);
            this.lblStatePoints.TabIndex = 20;
            this.lblStatePoints.Text = "0";
            // 
            // lblProvincePoints
            // 
            this.lblProvincePoints.AutoSize = true;
            this.lblProvincePoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvincePoints.Location = new System.Drawing.Point(185, 185);
            this.lblProvincePoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProvincePoints.Name = "lblProvincePoints";
            this.lblProvincePoints.Size = new System.Drawing.Size(16, 17);
            this.lblProvincePoints.TabIndex = 21;
            this.lblProvincePoints.Text = "0";
            // 
            // lblCountryPoints
            // 
            this.lblCountryPoints.AutoSize = true;
            this.lblCountryPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryPoints.Location = new System.Drawing.Point(185, 203);
            this.lblCountryPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCountryPoints.Name = "lblCountryPoints";
            this.lblCountryPoints.Size = new System.Drawing.Size(16, 17);
            this.lblCountryPoints.TabIndex = 22;
            this.lblCountryPoints.Text = "0";
            // 
            // lblClubPoints
            // 
            this.lblClubPoints.AutoSize = true;
            this.lblClubPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClubPoints.Location = new System.Drawing.Point(383, 168);
            this.lblClubPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClubPoints.Name = "lblClubPoints";
            this.lblClubPoints.Size = new System.Drawing.Size(16, 17);
            this.lblClubPoints.TabIndex = 23;
            this.lblClubPoints.Text = "0";
            // 
            // lblSchoolPoints
            // 
            this.lblSchoolPoints.AutoSize = true;
            this.lblSchoolPoints.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSchoolPoints.Location = new System.Drawing.Point(383, 185);
            this.lblSchoolPoints.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSchoolPoints.Name = "lblSchoolPoints";
            this.lblSchoolPoints.Size = new System.Drawing.Size(16, 17);
            this.lblSchoolPoints.TabIndex = 24;
            this.lblSchoolPoints.Text = "0";
            // 
            // lblStateQSOs
            // 
            this.lblStateQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateQSOs.Location = new System.Drawing.Point(17, 168);
            this.lblStateQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStateQSOs.Name = "lblStateQSOs";
            this.lblStateQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblStateQSOs.Size = new System.Drawing.Size(40, 16);
            this.lblStateQSOs.TabIndex = 25;
            this.lblStateQSOs.Text = "0";
            // 
            // lblProvinceQSOs
            // 
            this.lblProvinceQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProvinceQSOs.Location = new System.Drawing.Point(17, 185);
            this.lblProvinceQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblProvinceQSOs.Name = "lblProvinceQSOs";
            this.lblProvinceQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblProvinceQSOs.Size = new System.Drawing.Size(40, 16);
            this.lblProvinceQSOs.TabIndex = 26;
            this.lblProvinceQSOs.Text = "0";
            // 
            // lblCountryQSOs
            // 
            this.lblCountryQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryQSOs.Location = new System.Drawing.Point(17, 203);
            this.lblCountryQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCountryQSOs.Name = "lblCountryQSOs";
            this.lblCountryQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblCountryQSOs.Size = new System.Drawing.Size(40, 16);
            this.lblCountryQSOs.TabIndex = 27;
            this.lblCountryQSOs.Text = "0";
            // 
            // lblClubQSOs
            // 
            this.lblClubQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClubQSOs.Location = new System.Drawing.Point(219, 168);
            this.lblClubQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblClubQSOs.Name = "lblClubQSOs";
            this.lblClubQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblClubQSOs.Size = new System.Drawing.Size(40, 16);
            this.lblClubQSOs.TabIndex = 28;
            this.lblClubQSOs.Text = "0";
            // 
            // lblSchoolQSOs
            // 
            this.lblSchoolQSOs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSchoolQSOs.Location = new System.Drawing.Point(219, 185);
            this.lblSchoolQSOs.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSchoolQSOs.Name = "lblSchoolQSOs";
            this.lblSchoolQSOs.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.lblSchoolQSOs.Size = new System.Drawing.Size(40, 16);
            this.lblSchoolQSOs.TabIndex = 29;
            this.lblSchoolQSOs.Text = "0";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(61, 270);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(160, 20);
            this.label11.TabIndex = 30;
            this.label11.Text = "Claimed Score     =";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(61, 250);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(161, 20);
            this.label13.TabIndex = 32;
            this.label13.Text = "Calculated Score =";
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(61, 221);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 17);
            this.label15.TabIndex = 34;
            this.label15.Text = "Total multipliers  =              =";
            // 
            // lblTotalMultipliers
            // 
            this.lblTotalMultipliers.AutoSize = true;
            this.lblTotalMultipliers.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalMultipliers.Location = new System.Drawing.Point(184, 222);
            this.lblTotalMultipliers.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTotalMultipliers.Name = "lblTotalMultipliers";
            this.lblTotalMultipliers.Size = new System.Drawing.Size(16, 17);
            this.lblTotalMultipliers.TabIndex = 35;
            this.lblTotalMultipliers.Text = "0";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // lblCreatedBy
            // 
            this.lblCreatedBy.AutoSize = true;
            this.lblCreatedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreatedBy.Location = new System.Drawing.Point(9, 328);
            this.lblCreatedBy.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCreatedBy.Name = "lblCreatedBy";
            this.lblCreatedBy.Size = new System.Drawing.Size(77, 17);
            this.lblCreatedBy.TabIndex = 36;
            this.lblCreatedBy.Text = "Created by";
            // 
            // txtCreatedBy
            // 
            this.txtCreatedBy.BackColor = System.Drawing.SystemColors.Control;
            this.txtCreatedBy.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCreatedBy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCreatedBy.Location = new System.Drawing.Point(91, 330);
            this.txtCreatedBy.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCreatedBy.Name = "txtCreatedBy";
            this.txtCreatedBy.Size = new System.Drawing.Size(377, 16);
            this.txtCreatedBy.TabIndex = 38;
            this.txtCreatedBy.DoubleClick += new System.EventHandler(this.txtCreatedBy_DoubleClick);
            // 
            // txtCalculatedScore
            // 
            this.txtCalculatedScore.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtCalculatedScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCalculatedScore.Location = new System.Drawing.Point(221, 250);
            this.txtCalculatedScore.Margin = new System.Windows.Forms.Padding(2);
            this.txtCalculatedScore.Name = "txtCalculatedScore";
            this.txtCalculatedScore.ReadOnly = true;
            this.txtCalculatedScore.Size = new System.Drawing.Size(125, 19);
            this.txtCalculatedScore.TabIndex = 39;
            this.txtCalculatedScore.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtCalculatedScore_DoubleClick);
            // 
            // txtClaimedScore
            // 
            this.txtClaimedScore.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtClaimedScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClaimedScore.Location = new System.Drawing.Point(221, 270);
            this.txtClaimedScore.Margin = new System.Windows.Forms.Padding(2);
            this.txtClaimedScore.Name = "txtClaimedScore";
            this.txtClaimedScore.ReadOnly = true;
            this.txtClaimedScore.Size = new System.Drawing.Size(125, 19);
            this.txtClaimedScore.TabIndex = 40;
            this.txtClaimedScore.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtClaimedScore_DoubleClick);
            // 
            // txtEntryClass
            // 
            this.txtEntryClass.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtEntryClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEntryClass.Location = new System.Drawing.Point(276, 36);
            this.txtEntryClass.Margin = new System.Windows.Forms.Padding(2);
            this.txtEntryClass.Name = "txtEntryClass";
            this.txtEntryClass.ReadOnly = true;
            this.txtEntryClass.Size = new System.Drawing.Size(114, 19);
            this.txtEntryClass.TabIndex = 41;
            this.txtEntryClass.Text = "Entry Class";
            this.txtEntryClass.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.txtEntryClass_MouseDoubleClick);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(494, 380);
            this.Controls.Add(this.txtEntryClass);
            this.Controls.Add(this.txtClaimedScore);
            this.Controls.Add(this.txtCalculatedScore);
            this.Controls.Add(this.txtCreatedBy);
            this.Controls.Add(this.lblCreatedBy);
            this.Controls.Add(this.lblTotalMultipliers);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.lblSchoolQSOs);
            this.Controls.Add(this.lblClubQSOs);
            this.Controls.Add(this.lblCountryQSOs);
            this.Controls.Add(this.lblProvinceQSOs);
            this.Controls.Add(this.lblStateQSOs);
            this.Controls.Add(this.lblSchoolPoints);
            this.Controls.Add(this.lblClubPoints);
            this.Controls.Add(this.lblCountryPoints);
            this.Controls.Add(this.lblProvincePoints);
            this.Controls.Add(this.lblStatePoints);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.lblErrorCount);
            this.Controls.Add(this.lblQSOPoints);
            this.Controls.Add(this.lblCWPoints);
            this.Controls.Add(this.lblDigitalPoints);
            this.Controls.Add(this.lblPhonePoints);
            this.Controls.Add(this.lblTotalQSOs);
            this.Controls.Add(this.lblCWQSOs);
            this.Controls.Add(this.lblDigitalQSOs);
            this.Controls.Add(this.lblPhoneQSOs);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblEntryName);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.Text = "KWC SCR2 by Ken Gunther WB2KWC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
      private System.Windows.Forms.Label lblEntryName;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label lblPhoneQSOs;
      private System.Windows.Forms.Label lblDigitalQSOs;
      private System.Windows.Forms.Label lblCWQSOs;
      private System.Windows.Forms.Label lblTotalQSOs;
      private System.Windows.Forms.Label lblPhonePoints;
      private System.Windows.Forms.Label lblDigitalPoints;
      private System.Windows.Forms.Label lblCWPoints;
      private System.Windows.Forms.Label lblQSOPoints;
      private System.Windows.Forms.Label lblErrorCount;
      private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem loadStatesProvincesFileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem loadCountriesFileToolStripMenuItem;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label lblStatePoints;
      private System.Windows.Forms.Label lblProvincePoints;
      private System.Windows.Forms.Label lblCountryPoints;
      private System.Windows.Forms.Label lblClubPoints;
      private System.Windows.Forms.Label lblSchoolPoints;
      private System.Windows.Forms.Label lblStateQSOs;
      private System.Windows.Forms.Label lblProvinceQSOs;
      private System.Windows.Forms.Label lblCountryQSOs;
      private System.Windows.Forms.Label lblClubQSOs;
      private System.Windows.Forms.Label lblSchoolQSOs;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label lblTotalMultipliers;
      private System.Windows.Forms.OpenFileDialog openFileDialog1;
      private System.Windows.Forms.Label lblCreatedBy;
      private System.Windows.Forms.ToolStripMenuItem dumpToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem countriesListToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem statesprovincesListToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem provincesListToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem debugToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem resetLogFileToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem nowToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem autoresetToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem modeToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem cWToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem digitalToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem phoneToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem classToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem schoolToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem clubToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem individualToolStripMenuItem;
      private System.Windows.Forms.TextBox txtCreatedBy;
      public System.Windows.Forms.ToolStripMenuItem logCountryAssignmentToolStripMenuItem;
        private System.Windows.Forms.TextBox txtCalculatedScore;
        private System.Windows.Forms.TextBox txtClaimedScore;
        private System.Windows.Forms.TextBox txtEntryClass;
    }
}

