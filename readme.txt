  N3FJP Country File for AC Log 3.1 (and later)

------------------------------------------------------------------------


This is the source of the country file for logging software
written by Scott N3FJP.

*IMPORTANT*: This country file is *only* for users of AC Log 3.1 (and
later) and contesting programs whose versions are equal to or later than
the ones listed below under Supported Versions <#Supported Versions>.
This file contain more prefix and callsign mapping than the one for AC
Log 3.0 and earlier. In fact, if you try to use it with AC Log 3.0 and
earlier, your software will *not* work.

------------------------------------------------------------------------


    Installation Instructions

There are two ways to update the country and prefix files:

 1. If you are updating AC Log 3.2 or later, use this menu option:

        *Files -> Download Country Files From AD1C * 

    To update all of N3FJP's software, download all three files.

    Now you can re-start your logging program.
     
 2. Click on the links below to download the files that your program
    requires:
     

    ADIFCnt.txt
        contains the DXCC entities, both current and deleted. It also
        contains some "alias" names to be backward-compatible with older
        versions of the software. All programs use this file
     
    arrlpre2.txt
        contains the prefix and callsign mappings for all logging
        programs *except* CQWW
     
    cqwwpre2.txt
        contains the prefix and callsign mappings for CQWW

    When you are prompted, choose "Save File" (not "Open with...").
    Navigate to your N3FJP Software Shared directory, which is typically
    found here on a Windows XP system:

        %USERPROFILE%\My Documents\N3FJP Software\Shared

    On a Windows Vista or Windows 7 system, navigate to this directory
    instead:

        %USERPROFILE%\Documents\N3FJP Software\Shared

    When you try to save the file by clicking on OK, you should be
    prompted to over-write the existing file. If you do not get this
    prompt, you may be trying to save the file to the wrong place, or
    this is the first time you updated the files. You *must* update both
    files at the same time.

    Now you can re-start your logging program.

------------------------------------------------------------------------


    Checking your Installation

To verify that you have the new version of the files, try to log the
callsign VERSION and then look in the Country field in the logging
window (normally on the right). You should see a different country name
come up for each country file release. The expected country name can be
found in the Revision History <#History> below as the *Version entity*.
Don't save this "QSO", use Clear to erase the callsign.

To see if the country files installed correctly, click on Awards, then
"States, Counties, Countries Other". Leaving the "List only valid
entries" box checked, click on the Calculate button. In the Countries
section, the total of Worked + Remaining should match the number of
entities in the country file, which is also included in the Revision
History <#History> below. If it does not match, you may need to re-index
your log by clicking on the "Reset Counter" choice in the File menu. If
after doing that it still doesn't match, please seek help on the N3FJP
Software Users reflector
<http://groups.yahoo.com/group/N3FJP_Software_Users/>.

------------------------------------------------------------------------


    Supported Versions

These are the lowest version numbers that will work with this country file:

  * Amateur Contact Log 3.2
  * 10 Meter Contest Log 3.3
  * 10-10 QSO Party Log 2.8
  * ARRL 160 Meter Contest Log 2.5
  * ARRL RTTY Round-Up Contest Log 1.5
  * CQ 160 Meter Contest Log 2.6
  * CQ WPX Contest Log 2.8
  * CQ WPX Contest Network Log 2.6
  * CQ World Wide DX Contest Log 3.0
  * CQ World Wide DX Network Log 2.7
  * Field Day Contest Log 2.9
  * Field Day Network Log 2.6
  * FISTS Sprint Log 2.1
  * IARU HF Contest Log 2.2
  * International DX Contest Log 2.9
  * International DX Contest Log DX Version 1.8
  * International DX Contest Network Log 2.3
  * Kids Day Log 1.8
  * NAQP Contest Log 2.6
  * NAQP Contest Network Log 2.6
  * NA Sprint Contest Log 2.3
  * November Sweepstakes Contest Log 4.6
  * QRP ARCI Contest Log 2.4
  * RAC Contest Log 1.9
  * State QSO Party Logs
  * Stew Perry Contest Log 1.2
  * VHF Contest Log 2.9
  * VHF Network Log 2.4

------------------------------------------------------------------------
